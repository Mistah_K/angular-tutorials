import { convertToParamMap, ParamMap, Params } from '@angular/router';
import { ReplaySubject } from 'rxjs';

/**
 * Testing components that receive parameters from the router requires the use
 * of a test stub. We need a test stub instead of a simple spy in these cases for
 * the following reasons...
 *  - Some methods return values.
 *  - Some types required for testing need to be created using helper functions
 *  within the angular libraries.
 *  - Other components will need to mock the ActivateRoute service.
 * 
 * The last point is the most compelling reason to create a dedicated stub class;
 * we can reuse it wherever the stubbed service is used.
 */
export class ActivatedRouteStub {
    // Use a replay subject so that the observable can be "resubscribed" to.
    private subject = new ReplaySubject<ParamMap>();

    constructor(initialParams?: Params) {
        // Initialize the paramMap if params are supplied.
        this.setParamMap(initialParams);
    }

    // Return the replay subject as an observable. This is the property that 
    // subscribers will access.
    readonly paramMap = this.subject.asObservable();

    // Convert params to a paramMap and emit the new paramMap.
    public setParamMap(params?: Params) {
        // Use the convertToParamMap helper function to create a new paramMap.
        this.subject.next(convertToParamMap(params));
    }
}