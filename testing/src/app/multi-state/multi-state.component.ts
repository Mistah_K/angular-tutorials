import { Component, OnInit } from '@angular/core';

export enum ComponentState {
  NotStarted = "NotStarted",
  Started = "Started",
  Finished = "Finished"
}

@Component({
  selector: 'app-multi-state',
  templateUrl: './multi-state.component.html',
  styleUrls: ['./multi-state.component.css']
})
export class MultiStateComponent implements OnInit {
  public state: ComponentState;

  constructor() { }

  ngOnInit() {
  }

  public setState(state: string) {
    this.state = ComponentState[state];
  }
}
