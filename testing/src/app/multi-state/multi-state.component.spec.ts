import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiStateComponent } from './multi-state.component';
import { click } from '../../testing/click';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

/**
 * Example of testing a component that conditionally displays elements based off
 * of the component class' state.
 */
describe('MultiStateComponent', () => {
  let component: MultiStateComponent;
  let fixture: ComponentFixture<MultiStateComponent>;

  // Before each test, recreate the test module. I believe that compiling
  // components is asynchronous, which is why it needs to be wrapped in the async
  // function.
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiStateComponent ]
    })
    .compileComponents();
  }));

  // Additional setup code that resets the values of variables used in tests
  beforeEach(() => {
    fixture = TestBed.createComponent(MultiStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges(); // triggers binding
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // Example for querying the DOM and making assertion about a node's properties
  it('should display title as "Set the state!"', () => {
    // DebugElement is Angular's cross platform API for getting a handle on nodes
    // within the "DOM".
    let el: DebugElement = fixture.debugElement;
    // The "By" helper function enables querying the "DOM" using different
    // methods. Here we are using css selectors to query the "DOM".
    let title = el.query(By.css('h3'));
    // To get at the properties of the node, we need to unwrap the cross platform
    // representation into the native representation. Since we are testing for
    // the browser, we know that the native element is an HTMLElement.
    expect(title.nativeElement.textContent).toEqual('Set the state!')
  });

  // Example for simulating a click in the DOM.
  it('should display "Not Started" when in the NotStarted state', () => {
    // Get the debug element.
    let el: DebugElement = fixture.debugElement;
    // Query by css for children and get their nativeelements.
    let button: HTMLElement = el.query(By.css('#not-started-btn')).nativeElement;
    let display: HTMLElement = el.query(By.css('#state-display')).nativeElement;
    // Trigger the click using a helper function that abstracts the details of
    // dealing with a specific type of element.
    click(button);
    // Tell angular it needs to update the DOM. This is necessary in order for
    // binding to take place, which in turn updates the DOM.
    fixture.detectChanges();
    // Make your assertions.
    expect(display.innerText).toEqual('Not Started');
    expect(display.childElementCount).toEqual(1);
  });
});
