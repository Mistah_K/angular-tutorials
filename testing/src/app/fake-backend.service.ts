import { Injectable } from '@angular/core';

const profiles = {
  "tyler": {
    name: 'Tyler',
    sports: ['Soccer', 'Crossfit', 'Snowboarding'],
    foods: ['Asian', 'Mexican']
  },
  "cole": {
    name: 'Cole',
    sports: ['Cricket', 'Soccer'],
    foods: ['Pie', 'Cheeseburgers']
  }
}

@Injectable({
  providedIn: 'root'
})
export class FakeBackendService {
  constructor() { }

  public getProfileIds() {
    return Object.keys(profiles);
  }

  public getProfileById(id: string) {
    return profiles[id];
  }
}
