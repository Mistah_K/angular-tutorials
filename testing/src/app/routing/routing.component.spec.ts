import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RoutingComponent } from './routing.component';
import { By } from '@angular/platform-browser';
import { FakeBackendService } from '../fake-backend.service';
import { click } from '../../testing/click';

describe('RoutingComponent', () => {
  let component: RoutingComponent;
  let fixture: ComponentFixture<RoutingComponent>;
  let routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoutingComponent ],
      providers: [ 
        { provide: Router, useValue: routerSpy },
        FakeBackendService,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should route to /profile/{id} when one of its buttons are clicked', () => {
    // get id of first profile
    let id = component.ids[0];
    // get button by profile id
    let button = fixture.debugElement.query(By.css(`#${id}`));
    // simulate the click
    click(button);
    // get a handle on the spy method
    const spy = routerSpy.navigateByUrl as jasmine.Spy;
    // get arguments from first call to the spy method
    const navArgs = spy.calls.first().args[0];
    // assert that the router was invoked with the appropriate url
    expect(navArgs).toBe(`/profile/${id}`);
  });
});
