import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FakeBackendService } from '../fake-backend.service';

@Component({
  selector: 'app-routing',
  templateUrl: './routing.component.html',
  styleUrls: ['./routing.component.css']
})
export class RoutingComponent {
  public ids: string[];

  constructor(private router: Router, private fakeBackend: FakeBackendService) {
    this.ids = this.fakeBackend.getProfileIds();
  }

  public gotoDetails(id: string): void {
    this.router.navigateByUrl(`/profile/${id}`);
  }
}
