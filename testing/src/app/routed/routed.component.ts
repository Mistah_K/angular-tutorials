import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FakeBackendService } from '../fake-backend.service';

@Component({
  selector: 'app-routed',
  templateUrl: './routed.component.html',
  styleUrls: ['./routed.component.css']
})
export class RoutedComponent implements OnInit {
  public profile: any;

  constructor(private activatedRoute: ActivatedRoute, 
    private fakeBackend: FakeBackendService) {
      this.activatedRoute.paramMap.subscribe(
        value => {
          this.profile = fakeBackend.getProfileById(value.get('id'));
        }
      )
  }

  ngOnInit() {
  }

}
