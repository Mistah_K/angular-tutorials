import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { By } from '@angular/platform-browser';

import { RoutedComponent } from './routed.component';
import { ActivatedRouteStub } from '../../testing/activated-route-stub';
import { FakeBackendService } from '../fake-backend.service';

describe('RoutedComponent', () => {
  let component: RoutedComponent;
  let fixture: ComponentFixture<RoutedComponent>;
  // Create new instances of stub services.
  let activatedRouteStub = new ActivatedRouteStub();
  let fakeBackendService = new FakeBackendService();
  // Variables
  let ids: string[];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoutedComponent ],
      providers: [ 
        // Pass already instantiated stub services to the module's injector.
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: FakeBackendService, useValue: fakeBackendService }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    // Init the ActivatedRoute before the component (this must happen first
    // because the ActivatedRouteStub class only initializes the paramMap if
    // params are passed to the constructor).
    ids = fakeBackendService.getProfileIds();
    activatedRouteStub.setParamMap({ id: ids[0] });
    // init the component
    fixture = TestBed.createComponent(RoutedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display the profile for the user id in the route', () => {
    let profile = fakeBackendService.getProfileById(ids[0]);
    let username = fixture.debugElement.query(By.css('#username'));
    expect(username.nativeElement.innerText).toEqual(profile.name);
  });

  it('should display the profile for a different user id', () => {
    // Reset the ActivatedRoute's paramMap and detect changes.
    activatedRouteStub.setParamMap({ id: ids[1] });
    fixture.detectChanges();
    // Get the new profile and make sure it is displayed.
    let profile = fakeBackendService.getProfileById(ids[1]);
    let username = fixture.debugElement.query(By.css('#username'));
    expect(username.nativeElement.innerText).toEqual(profile.name);
  });
});
