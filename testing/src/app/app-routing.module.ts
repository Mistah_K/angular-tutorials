import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { RoutingComponent } from './routing/routing.component';
import { RoutedComponent } from './routed/routed.component';
import { MultiStateComponent } from './multi-state/multi-state.component';

const routes = [
    { path: '', component: HomeComponent },
    { path: 'multi-state', component: MultiStateComponent },
    { path: 'profiles', component: RoutingComponent },
    { path: 'profile/:id', component: RoutedComponent },
]

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}